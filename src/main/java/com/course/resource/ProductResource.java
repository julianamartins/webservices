package com.course.resource;

import com.course.entities.Product;
import com.course.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/products")
@CrossOrigin
public class ProductResource {

    @Autowired
    private ProductService product;

    @GetMapping
    public ResponseEntity<List<Product>> findAll() {
        List<Product> list = product.findAll();
        return ResponseEntity.ok().body(list);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Product> findById(@PathVariable Long id) {
        Product obj = product.findById(id);
        return ResponseEntity.ok(obj);
    }
}
