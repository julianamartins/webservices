package com.course.resource;

import com.course.entities.Category;
import com.course.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/categories")
@CrossOrigin
public class CategoryResource {

    @Autowired
    private CategoryService category;

    @GetMapping
    public ResponseEntity<List<Category>> findAll() {
        List<Category> list = category.findAll();
        return ResponseEntity.ok().body(list);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Category> findById(@PathVariable Long id) {
        Category obj = category.findById(id);
        return ResponseEntity.ok(obj);
    }

}
